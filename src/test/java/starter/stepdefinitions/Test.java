package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class Test {
    Response response;
    @When("he calls endpoint {string}")
    public void he_calls_endpoint(String product) {
         response = given().accept(ContentType.JSON).when().get(baseURI + product);
    }
    @Then("he sees the {string}")
    public void he_sees_the(String expectedProduct) {
        response.then().body("title",everyItem(containsStringIgnoringCase(expectedProduct)));
    }
    @Then("he see result detail {string}")
    public void he_see_result_detail(String error) {
        response.then().body("detail.error",is(true));
    }
}
