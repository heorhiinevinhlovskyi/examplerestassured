package starter.utilities;

import io.cucumber.java.Before;
import io.restassured.RestAssured;

public class BaseURI {
    @Before("@api")
    public static void init(){
        RestAssured.baseURI = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";
    }

}
